<?php

/**
 * @file
 * Documentation for Node Import Soap module's hook.
 */

/**
 * Perform steps required to import content.
 *
 * This hook is invoked before the node is saved.
 *
 * @param object $node
 *   Content where the information obtained from the SOAP server will be saved.
 * @param object $importer
 *   Meta-information about the importer set.
 * @param array $row
 *   Source data set.
 *
 * @return array
 *   Array of error messages.
 */
function hook_node_import_soap_row_presave(&$node, $importer, $row) {
  static $types, $fieldinfo = array();
  $errors = array();

  if (!isset($types)) {
    $types = (array) content_types();
  }

  module_load_include('inc', 'filefield', 'field_file');
  foreach ($types[$importer->content_type]['fields'] as $field_key => $label) {
    if ($label['type'] == 'filefield') {
      if (!empty($node->$field_key)) {
        module_load_include('inc', 'filefield', 'filefield_field');
        foreach ($node->$field_key as $old_file) {
          field_file_delete($old_file, TRUE);
        }
        unset($node->$field_key);
      }

      $source_key = $field_key . '_sourcefile';

      // Force a node value for missing data.
      if (empty($node->$source_key)) {
        $values = array();
      }
      else {
        // Explode multiple values to create the $delta and $value for each.
        if ($label['multiple']) {
          $values = $node->$source_key;
        }
        else {
          $values = array(0 => $node->$source_key);
        }
      }

      foreach ($values as $key => $urlpath) {
        $source_file = $urlpath;
        $filename = array_pop(explode('/', $source_file));
        if (!empty($filename) && @copy($source_file, file_directory_temp() . '/' . $filename)) {
          $field_name = $field_key;
          $field = content_fields($field_name, $importer->content_type);
          $validators = array();
          if ($field['widget']['type'] == 'imagefield_widget') {
            $validators = array_merge($validators, imagefield_widget_upload_validators($field));
          }
          $files_path = file_directory_path();
          if ($file = field_file_save_file(file_directory_temp() . '/' . $filename, $validators, $files_path)) {
            $file['list'] = 1;
            if (!isset($node->$field_name)) {
              $node->$field_name = array();
            }
            array_push($node->$field_name, $file);
          }
          else {
            $errors[] = t('File save failed for %file', array('%file' => $source_file));
          }
        }
        else {
          array_push($node->$field_name, array(''));
        }
      }

      if (isset($node->$source_key)) {
        unset($node->$source_key);
      }

    }
  }
  return $errors;

}

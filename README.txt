
-- SUMMARY --

The node import SOAP module allow you to import content from a SOAP service
allowing to define primary keys and foreign keys between different functions.

-- FEATURES --

* Fetch SOAP data making it available for processing.
* Works with WS-Security
* Define primary and foreign keys between various functions of the SOAP server.
* Import content by cron.
* Update previously imported content.
* Added hook Presave.

-- REQUIREMENTS --

The following modules are required:

* SOAP Client, http://drupal.org/project/soapclient
* Token, http://drupal.org/project/token


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure Importers in Administer >> Site building >> Importer Soap

<?php

/**
 * @file
 * Node import SOAP admin interface
 */

/**
 * Page node importer list.
 */
function node_import_soap_admin_page() {
  $headers = array(
    t('Name'),
    t('Description'),
    t('Cron enabled'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $result = db_query("SELECT id, config FROM {nis_importer} ORDER BY id ASC");
  $rows = array();
  while ($item = db_fetch_object($result)) {
    $import = unserialize($item->config);

    $rows[] = array(
      l($import->name, 'admin/build/imports/' . $item->id . '/edit/basic'),
      check_plain($import->description),
      ($import->cron_enabled) ? t('Yes') : t('No'),
      l(t('Edit'), 'admin/build/imports/' . $item->id . '/edit'),
      l(t('Import'), 'admin/build/imports/' . $item->id . '/import'),
      l(t('Delete'), 'admin/build/imports/' . $item->id . '/delete'),
    );
  }

  $output = theme('table', $headers, $rows);
  return $output;

}

/**
 * Add/Edit new importer form.
 */
function node_import_soap_import_form(&$form_state, $import = NULL) {

  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Import name'),
    '#description' => t('Import name'),
    '#default_value' => $import->name,
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $import->description,
    '#description' => t('Description of the Importer'),
  );

  $form['cron_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cron enabled'),
    '#description' => t('Import content cron'),
    '#default_value' => $import->cron_enabled,
  );

  $form['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_get_types('names'),
    '#default_value' => $import->content_type,
    '#description' => t('Select the content type for the nodes to be created.'),
    '#required' => TRUE,
  );

  $form['soap_server'] = array(
    '#type' => 'textfield',
    '#title' => t('SOAP server endpoint URL'),
    '#description' => t('Enter the absolute endpoint URL of the SOAP Server service. If WSDL is being used (see SOAPFetcher settings), this will be the URL to retrieve the WSDL.'),
    '#default_value' => $import->soap_server,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

/**
 * Handle Save/Modify import form.
 */
function node_import_soap_import_form_submit($form, &$form_state) {

  // Edit group.
  $import = ($form['#parameters'][2]) ? $form['#parameters'][2] : new stdClass();
  $import->name = $form_state['values']['name'];
  $import->description = $form_state['values']['description'];
  $import->cron_enabled = $form_state['values']['cron_enabled'];
  $import->soap_server = $form_state['values']['soap_server'];
  $import->content_type = $form_state['values']['content_type'];
  $db_import = array('config' => $import);

  // Edit group.
  if ($form['#parameters'][2]) {
    $db_import['id']  = $form['#parameters'][2]->id;
    $key = 'id';
    drupal_set_message(t('Importer @import has been updated.', array('@import' => $import->name)));
  }
  else {
    drupal_set_message(t('Importer @import has been created.', array('@import' => $import->name)));
  }

  drupal_write_record('nis_importer', $db_import, $key);
}

/**
 * Delete importer info.
 */
function node_import_soap_import_delete_form(&$form_state, $import = NULL) {
  $form = array();

  return confirm_form($form, t('Are you sure that you want to delete the importer @name?',
    array('@name' => $import->name)), 'admin/build/imports');
}

/**
 * Submit handler to delete a importer.
 */
function node_import_soap_import_delete_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;

  if ($importer) {
    db_query('DELETE FROM {nis_importer} WHERE id = %d', $importer->id);
    drupal_set_message(t('@importer has been deleted.', array('@importer' => $importer->name)));
  }
  $form_state['redirect'] = 'admin/build/imports';

}

/**
 * Add/Edit new function form.
 */
function node_import_soap_function_form(&$form_state, $import = NULL, $function = NULL) {
  $form = array();
  $form['name_function'] = array(
    '#type' => 'textfield',
    '#title' => t('SOAP Function'),
    '#description' => t('SOAP function name'),
    '#default_value' => $function,
    '#required' => TRUE,
  );

  $form['xpath_root'] = array(
    '#type' => 'textfield',
    '#title' => t('Xpath root'),
    '#description' => t('XPath root of node'),
    '#default_value' => $import->functions['functions'][$function]['xpath_root'],
    '#required' => TRUE,
  );

  $form['main_function'] = array(
    '#type' => 'checkbox',
    '#title' => t('Main function'),
    '#description' => t('Main list where the nodes are obtained'),
    '#default_value' => $function == $import->functions['main_function'] & $function != NULL,
  );

  $form['arguments'] = array(
    '#type' => 'textarea',
    '#title' => t('Arguments'),
    '#description' => t('Enter the arguments of the function. One argument per line, for named arguments, the format of name=value may be used.'),
    '#default_value' => $import->functions['functions'][$function]['arguments'],
    '#suffix' => theme('token_tree', array('node import soap'), FALSE, TRUE),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Submit handler to add/edit a function.
 */
function node_import_soap_function_form_submit($form, &$form_state) {
  $import = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;

  if (array_key_exists('#parameters', $form) && $form['#parameters'][3]) {
    unset($import->functions['functions'][$form['#parameters'][3]]);
  }

  if ($form_state['values']['main_function']) {
    $import->functions['main_function'] = $form_state['values']['name_function'];
  }

  $import->functions['functions'][$form_state['values']['name_function']]['arguments'] = $form_state['values']['arguments'];
  $import->functions['functions'][$form_state['values']['name_function']]['xpath_root'] = $form_state['values']['xpath_root'];

  $id = $import->id;

  unset($import->id);

  $db_import = array(
    'id' => $id,
    'config' => $import,
  );

  drupal_write_record('nis_importer', $db_import, 'id');

  // Edit group.
  if (array_key_exists('#parameters', $form) && $form['#parameters'][3]) {
    drupal_set_message(t('Function @function has been updated.', array('@function' => $form['#parameters'][3])));
  }
  else {
    drupal_set_message(t('Function @function has been created.', array('@function' => $form_state['values']['name_function'])));
  }

  $form_state['redirect'] = 'admin/build/imports/' . $id . '/edit/functions-list';
}

/**
 * Main function adminitration page.
 */
function node_import_soap_functions_page($import) {

  $headers = array(
    t('Name'),
    t('Main function'),
    t('Xpath root node'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  $result = $import->functions['functions'];
  $rows = array();
  if ($result) {
    foreach ($result as $key => $value) {
      $rows[] = array(
        check_plain($key),
        $import->functions['main_function'] == $key ? t('Yes') : t('No'),
        check_plain($value['xpath_root']),
        l(t('Edit'), 'admin/build/imports/' . $import->id . '/' . $key . '/edit'),
        l(t('Fields'), 'admin/build/imports/' . $import->id . '/' . $key . '/fields'),
        l(t('Delete'), 'admin/build/imports/' . $import->id . '/' . $key . '/delete'),
      );
    }
  }

  $output = theme('table', $headers, $rows);

  return $output;
}

/**
 * Submit handler to delete a function.
 */
function node_import_soap_function_delete_form(&$form_state, $import = NULL, $function = NULL) {

  $form = array();

  return confirm_form($form, t('Are you sure that you want to delete the function @name?',
    array('@name' => $function)), 'admin/build/imports/' . $import->id . '/edit/functions-list');
}

/**
 * Submit handler to delete a function.
 */
function node_import_soap_function_delete_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $function = array_key_exists('#parameters', $form) ? $form['#parameters'][3] : NULL;
  $id = $importer->id;

  // Delete id.
  unset($importer->id);
  // Delete function.
  unset($importer->functions['functions'][$function]);

  $db_import = array(
    'id' => $id,
    'config' => $importer,
  );

  drupal_write_record('nis_importer', $db_import, 'id');

  drupal_set_message(t('@function has been deleted.', array('@function' => $function)));
  $form_state['redirect'] = 'admin/build/imports/' . $id . '/edit/functions-list';

}

/**
 * Add/Edit new fields form.
 */
function node_import_soap_function_fields_form(&$form_state, $import = NULL, $function = NULL, $field = NULL) {
  $form = array();

  module_load_include('inc', 'node_import_soap');

  $form['field_name'] = array(
    '#type' => 'textfield',
    '#default_value' => $field,
    '#size' => 40,
    '#required' => TRUE,
    '#element_validate' => array('_node_import_soap_is_unique'),
  );

  $form['importer_id'] = array(
    '#type' => 'hidden',
    '#value' => $import->id,
  );

  $form['xpath'] = array(
    '#type' => 'textfield',
    '#default_value' => $import->functions['functions']['fields'][$field]['xpath'],
    '#size' => 40,
    '#required' => TRUE,
  );

  $form['primary_key'] = array(
    '#type' => 'checkbox',
    '#default_value' => $import->functions['functions']['fields'][$field]['primary_key'],
  );

  $form['foreign_key'] = array(
    '#type' => 'select',
    '#options' => _node_import_soap_foreign_key($import, $function),
    '#default_value' => 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler to add/edit fields.
 */
function node_import_soap_function_fields_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $function = array_key_exists('#parameters', $form) ? $form['#parameters'][3] : NULL;

  $importer->functions['fields'][$form_state['values']['field_name']] = array(
    'xpath' => $form_state['values']['xpath'],
    'function' => $function,
    'primary_key' => $form_state['values']['primary_key'],
    'foreign_key' => $form_state['values']['foreign_key'],
  );

  $id = $importer->id;

  // Delete id.
  unset($importer->id);

  $db_import = array(
    'id' => $id,
    'config' => $importer,
  );

  drupal_write_record('nis_importer', $db_import, 'id');

}

/**
 * Form add/edit mapping from content fields to fields of functions.
 */
function node_import_soap_mapping_form(&$form_state, $import = NULL) {

  module_load_include('inc', 'node_import_soap');

  $form = array();
  $content_type = $import->content_type;
  $mapping = (!empty($import->mapping)) ? $import->mapping : array();

  $form['destination_field'] = array(
    '#type' => 'select',
    '#options' => _node_import_soap_get_fields($content_type, array_keys($mapping)) ,
  );

  $form['source_field'] = array(
    '#type' => 'select',
    '#options' => _node_import_soap_get_functions_fields($import, _node_import_soap_array_get_values('source', $mapping)),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

/**
 * Submit handler mapping fields.
 */
function node_import_soap_mapping_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $importer->mapping[$form_state['values']['destination_field']] = array(
    'destination' => $form_state['values']['destination_field'],
    'source' => $form_state['values']['source_field'],
  );

  $id = $importer->id;

  // Delete id.
  unset($importer->id);

  $db_import = array(
    'id' => $id,
    'config' => $importer,
  );

  drupal_write_record('nis_importer', $db_import, 'id');
}

/**
 * Theme the new-mapping form.
 */
function theme_node_import_soap_mapping_form($form) {
  module_load_include('inc', 'node_import_soap');

  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $header = array(t('Destination field'), t('Source field'), t('Operations'));
  $rows = array();

  if ($importer->mapping) {
    $fields = _node_import_soap_get_fields($importer->content_type);
    $sources = _node_import_soap_get_functions_fields($importer);
    foreach ($importer->mapping as $field => $value) {
      $rows[] = array(
        check_plain($fields[$field]),
        check_plain($sources[$value['source']]),
        l(t('Delete'), 'admin/build/imports/' . $importer->id . '/mapping/' . $field . '/delete'),
      );
    }
  }

  $rows[] = array(
    drupal_render($form['destination_field']),
    drupal_render($form['source_field']),
    drupal_render($form['submit']),
  );

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows);

  return $output;

}

/**
 * Theme function fields form.
 */
function theme_node_import_soap_function_fields_form($form) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $function = array_key_exists('#parameters', $form) ? $form['#parameters'][3] : NULL;

  $header = array(
    t('Id'),
    t('xpath SOAP'),
    t('Primary key'),
    t('Foreign key'),
    t('Operations'),
  );

  $rows = array();
  if ($importer->functions['fields']) {
    foreach ($importer->functions['fields'] as $field => $value) {
      if ($value['function'] == $function) {
        $rows[] = array(
          $field,
          check_plain($value['xpath']),
          ($value['primary_key']) ? t('Yes') : t('No'),
          ($value['foreign_key']) ? check_plain($value['foreign_key']) . ' (' . check_plain($importer->functions['fields'][$value['foreign_key']]['function']) . ')' : NULL,
          l(t('Delete'), 'admin/build/imports/' . $importer->id . '/' . $value['function'] . '/' . $field . '/delete'),
        );
      }
    }
  }

  $rows[] = array(
    drupal_render($form['field_name']),
    drupal_render($form['xpath']),
    drupal_render($form['primary_key']),
    drupal_render($form['foreign_key']),
    drupal_render($form['submit']),
  );

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows);

  return $output;

}

/**
 * Delete mapping form.
 */
function node_import_soap_mapping_delete_form(&$form_state, $import = NULL, $field = NULL) {
  $form = array();

  return confirm_form($form, t('Are you sure that you want to delete the field @name?',
    array('@name' => $field)), 'admin/build/imports/' . $import->id . '/edit/mapping');
}

/**
 * Delete handler mapping.
 */
function node_import_soap_mapping_delete_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $field = array_key_exists('#parameters', $form) ? $form['#parameters'][3] : NULL;
  $id = $importer->id;

  // Delete id.
  unset($importer->id);

  // Delete mapping.
  unset($importer->mapping[$field]);

  $db_import = array(
    'id' => $id,
    'config' => $importer,
  );

  drupal_write_record('nis_importer', $db_import, 'id');

  drupal_set_message(t('@field has been deleted.', array('@field' => $field)));

  $form_state['redirect'] = 'admin/build/imports/' . $id . '/edit/mapping';

}

/**
 * Delete field from function and mapping form.
 */
function node_import_soap_importer_field_delete_form(&$form_state, $import = NULL, $function = NULL, $field = NULL) {
  $form = array();

  return confirm_form($form, t('Are you sure that you want to delete the field @name?',
    array('@name' => $field)), 'admin/build/imports/' . $import->id . '/edit/mapping');
}

/**
 * Handler for delete field from function and mapping form.
 */
function node_import_soap_importer_field_delete_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;
  $function = array_key_exists('#parameters', $form) ? $form['#parameters'][3] : NULL;
  $field = array_key_exists('#parameters', $form) ? $form['#parameters'][4] : NULL;
  $id = $importer->id;

  // Delete id.
  unset($importer->id);

  if ($importer->functions['fields'][$field]['primary_key']) {
    // Delete posibles references to primary_key.
    foreach ($importer->functions['fields'] as $name_field => $value) {
      if ($value['foreign_key'] == $field) {
        $importer->functions['fields'][$name_field]['foreign_key'] = FALSE;
      }
    }
  }

  // Delete field from function.
  unset($importer->functions['fields'][$field]);

  // Delete mapping that contain like source this field.
  foreach ($importer->mapping as $cck_field => $value) {
    if ($value['source'] == $field) {
      unset($importer->mapping[$cck_field]);
    }
  }

  $db_import = array(
    'id' => $id,
    'config' => $importer,
  );

  drupal_write_record('nis_importer', $db_import, 'id');

  drupal_set_message(t('@field has been deleted.', array('@field' => $field)));

  $form_state['redirect'] = 'admin/build/imports/' . $id . '/' . $function . '/fields';
}

/**
 * Import content form.
 */
function node_import_soap_import_import_form(&$form_state, $import = NULL) {
  $form = array();

  return confirm_form($form, t('Are you sure that you want to import @name?',
    array('@name' => $import->name)), 'admin/build/imports');
}

/**
 * Handler import content.
 */
function node_import_soap_import_import_form_submit($form, &$form_state) {
  $importer = array_key_exists('#parameters', $form) ? $form['#parameters'][2] : NULL;

  // Import nodes from SOAP importer.
  node_import_soap_import($importer);

  drupal_set_message(t('The import @import_name was successful.', array('@import_name' => $importer->name)));

  $form_state['redirect'] = 'admin/build/imports';

}

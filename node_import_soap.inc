<?php

/**
 * @file
 * Node Import soap module: Helper functions.
 */

/**
 * Help function that returns the foreign keys in a particular function.
 *
 * @param stdClass $importer
 *   Importer object.
 *
 * @param sring $function
 *   Function name.
 *
 * @return array
 *   Foreigns keys functions.
 */
function _node_import_soap_foreign_key($importer, $function) {
  $fk = array(0 => '');

  if (array_key_exists('fields', $importer->functions)) {
    foreach ($importer->functions['fields'] as $key => $value) {
      if ($value['primary_key'] && $value['function'] != $function) {
        $fk[$key] = $key . ' (' . $value['function'] . ')';
      }
    }
  }
  return $fk;
}

/**
 * Validate form field name.
 */
function _node_import_soap_is_unique($element, &$form_state) {

  $importer = node_import_soap_importer_load($form_state['values']['importer_id']);
  $fields = array_key_exists('fields', $importer->functions) ? $importer->functions['fields'] : array();

  if (array_key_exists($form_state['values']['field_name'], $fields)) {
    form_error($element, t('Id must be unique.'));
  }
}

/**
 * Helper get fields content type.
 *
 * @param string $type
 *   Content type.
 * @param array  $nofields
 *   Fields will not be taken into account.
 *
 * @return array
 *   Fields to the content type.
 */
function _node_import_soap_get_fields($type, $nofields = array()) {
  $fields = array();

  $drupal_types['fields'] = array(
    'title' => array('widget' => array('label' => t("Drupal Title"))),
    'body' => array('widget' => array('label' => t("Drupal Body"))),
  );

  $infotype = array_merge_recursive($drupal_types, content_types($type));

  foreach ($infotype['fields'] as $field => $info) {
    if (!in_array($field, $nofields)) {
      $fields[$field] = $info['widget']['label'] . ' (' . $field . ')';
    }

  }

  return $fields;
}

/**
 * Helper get fields importer.
 *
 * @param stdClass $importer
 *   Importer object.
 * @param array  $nofields
 *   Fields will not be taken into account.
 *
 * @return array
 *   Fields to the importer.
 */
function _node_import_soap_get_functions_fields($importer, $nofields = array()) {
  $fields = array();

  if (isset($importer->functions) && array_key_exists('fields', $importer->functions)) {
    foreach ($importer->functions['fields'] as $key => $value) {
      if (!in_array($key, $nofields)) {
        $fields[$key] = $key . ' (' . $value['function'] . ')';
      }
    }
  }

  return $fields;
}

/**
 * Helper gets all the elements of an array of a specified key.
 *
 * @param string $key
 *   Key string
 * @param array $array
 *   Search array
 *
 * @return array
 *   Array key
 */
function _node_import_soap_array_get_values($key, $array) {
  $tmp = array();

  foreach ($array as $key2 => $value) {
    $tmp[] = $value[$key];
  }

  return $tmp;
}

/**
 * Returns the function call.
 *
 * @param mixed $soap_server
 *   SOAP server object.
 * @param mixed $importer
 *   Importer object.
 * @param string $function
 *   Name function call.
 * @param array $value
 *   Tokens value for this function.
 *
 * @return array
 *   Result call function.
 */
function _node_import_soap_get_soap_rows($soap_server, &$importer, $function, $value = NULL) {
  // Call Soap server.
  $options = $importer->functions['functions'][$function];
  $args = _node_import_soap_get_arguments($options['arguments'], $value);
  $result = $soap_server['#return']->call($function, $args);
  // Gets an array with the values of the call to a SOAP function.
  $return = _node_import_soap_get_path_value($result['#return'], $options['xpath_root']);

  return _node_import_soap_rows_nodes($return);

}

/**
 * Gets a string with the following format VAR=VALUE parameters.
 *
 * @param string $arguments
 *   Parameters in format VAR=VALUE.
 * @param array $token
 *   Set of tokens to apply.
 *
 * @return array
 *   Parameters.
 */
function _node_import_soap_get_arguments($arguments, $token = NULL) {
  $args = array();

  if (empty($arguments)) {
    return $args;
  }

  $param_str = preg_replace('/[\s]*=[\s]*/', '=', $arguments);
  $params    = explode("\n", $param_str);

  foreach ($params as $param) {
    $param_list = explode('=', $param);
    $values_list = array();
    $value = token_replace(array_pop($param_list), 'node import soap', $token);
    while ($name = array_pop($param_list)) {
      $values_list[$name] = $value;
      $value = $values_list;
      $values_list = array();
    }
    $key = array_keys($value);
    $args[$key[0]] = $value[$key[0]];
  }

  return $args;
}

/**
 * Get value an array by using "root/branch/leaf" notation.
 *
 * @param array $array
 *   Array with notation [root][branch][leaf].
 * @param string $path
 *   Path in notation root/branch/leaf
 * @param mixed $default
 *   Default value to be returned.
 *
 * @return mixed
 *   Contents of the array in this notation.
 */
function _node_import_soap_get_path_value($array, $path, $default = NULL) {

  // Extract parts of the path.
  $parts = explode('/', $path);
  $value = $array;

  if (count($parts) == 1) {
    if ($parts[0] == '*') {
      return $array;
    }
    else {
      return $array[$parts[0]];
    }
  }
  else {
    $key = array_shift($parts);
    $path = implode('/', $parts);

    if ($key == '*') {
      $valor = array();
      foreach ($array as $key => $value) {
        $valor[$key] = _node_import_soap_get_path_value($value, $path, $default);
      }

      return $valor;
    }
    else {
      $array = $array[$key];

      return _node_import_soap_get_path_value($array, $path, $default);
    }
  }
}

/**
 * Return rows resource.
 *
 * @param array $array
 *   An array with keys that may be numerical or not.
 *
 * @return array
 *   An array with numeric keys.
 */
function _node_import_soap_rows_nodes($array) {

  if (array_key_exists(0, $array)) {
    return $array;
  }
  else {
    return array($array);
  }
}

/**
 * Return keys primary or foreign  of importer.
 *
 * @param stdClass $importer
 *   Importer Object.
 * @param string $key
 *   primary_key or foreign_key.
 * @param string $function
 *   SOAP function name.
 *
 * @return array
 *   Fields name for a primary or foreing key.
 */
function _node_import_soap_get_key($importer, $key = 'primary_key', $function = NULL) {

  $result = array();

  if (!array_key_exists('fields', (array) $importer->functions)) {
    return $result;
  }

  foreach ($importer->functions['fields'] as $field_key => $field_value) {
    if ($field_value[$key]) {
      if ($field_value['function'] == $function) {
        return $field_key;
      }

      $result[$field_key] = $field_value['function'];
    }
  }

  return $result;
}

/**
 * Recursively returns the foreign key values.
 *
 * @param stdClass $importer
 *   Importer Object.
 * @param mixed $soap_server
 *   SOAP server conecction.
 * @param array $functions
 *   Functions importer SOAP
 * @param string $function
 *   Function name.
 * @param array $source
 *   foreing_key values result.
 * @param string $foreign_name
 *   Name foreign key.
 *
 * @return array
 *   Array foreing key values.
 */
function _node_import_soap_get_foreign_key_values($importer, $soap_server, $functions, $function, &$source, $foreign_name = NULL) {
  if (!array_key_exists('foreign_key', $functions[$function])) {
    return _node_import_soap_get_foreign_key_result_values($importer, $soap_server, $functions, $function, $source, $foreign_name);
  }
  else {
    $result = array();
    foreach ($functions[$function]['foreign_key'] as $key => $value) {
      $result = array_merge_recursive($result, _node_import_soap_get_foreign_key_values($importer, $soap_server, $functions, $value['function'], $source, $key));
      return $result;
    }
  }
}

/**
 * Gets the values of a key foreign calling the corresponding function.
 *
 * @param mixed $importer
 *   Importer object.
 * @param mixed $soap_server
 *   Soap server object.
 * @param array $functions
 *   Function description from soap server.
 * @param string $function
 *   Function which will be called.
 * @param array $source
 *   Information function.
 * @param string $foreign_name
 *   The name of the foreign key for this function.
 *
 * @return array
 *   Row in notation key => value.
 */
function _node_import_soap_get_foreign_key_result_values($importer, $soap_server, $functions, $function, $source, $foreign_name) {

  if (empty($foreign_name)) {
    return array();
  }

  $parameters = $source[$foreign_name];
  $function_info = $functions[$function];

  $rows = array();
  // It's an array of parameters?
  if (is_array($parameters)) {
    foreach ($parameters as $value) {
      // The value reaching the function using the key foreign tokens.
      $rows = array_merge_recursive(
        $rows,
        _node_import_soap_search_key_value(_node_import_soap_get_soap_rows($soap_server, $importer, $function, $value),
        $function_info, $value));
    }
  }
  else {
    // Comes only a single parameter.
    $value = $parameters;
    $rows = _node_import_soap_search_key_value(
      _node_import_soap_get_soap_rows($soap_server, $importer, $function, $value),
      $function_info,
      $value);
  }

  return $rows;
}

/**
 * Search for value in rows by primary key.
 *
 * @param array $rows
 *   Rows for search primary key value.
 * @param array $function_info
 *   Function information.
 * @param mixed $value
 *   Search value.
 *
 * @return array
 *   Row that matches the searched value.
 */
function _node_import_soap_search_key_value($rows, $function_info, $value) {
  $pk = $function_info['primary_key'][0];
  $fields = $function_info['fields'];
  foreach ($rows as $key => $info) {
    if (_node_import_soap_get_path_value($info, $fields[$pk]['xpath']) == $value) {
      $row = array();
      foreach ($fields as $name => $v) {
        $row[$name] = _node_import_soap_get_path_value($info, $fields[$name]['xpath']);
      }

      return $row;
    }
  }

  $row = array();
  foreach ($fields as $name => $v) {
    if ($pk == $name) {
      $row[$pk] = $value;
    }
    else {
      $row[$name] = NULL;
    }
  }

  return $row;
}

/**
 * It reorders the functions divided into fields, primary keys, and foreign key.
 *
 * @param mixed $importer
 *   Importer object.
 *
 * @return array
 *   Information on the functions sorted by key.
 */
function _node_import_soap_get_functions_info($importer) {
  $info = array();

  foreach ($importer->functions['fields'] as $field => $value) {
    $info[$value['function']]['fields'][$field] = array('name' => $field, 'xpath' => $value['xpath']);

    if ($value['primary_key']) {
      $info[$value['function']]['primary_key'][] = $field;
    }

    if ($value['foreign_key']) {
      $info[$value['function']]['foreign_key'][$field] = $importer->functions['fields'][$value['foreign_key']];
    }
  }

  return $info;
}
